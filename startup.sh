docker run -d \
  --name FiveM \
  --restart=on-failure \
  -e LICENCE_KEY=bm2a1ehltnxgyez5lhh7dte6zpjkua8w \
  -p 30120:30120 \
  -p 30120:30120/udp \
  -v /volumes/fivem:/config \
  -ti \
  spritsail/fivem
